# OrionUO.Scripts
Run.sh generate Autoload.oajs and Kraft.oajs based on *.oajs scripts

[Orion gitlab](https://github.com/Hotride/OrionUO/blob/master/README.md)


Tutorial:
1. [Install Orion Launcher](http://orionuo.online/)
2. Select Erebor Folder
3. Select Folder for Orion Client files ( in my case also Erebor folder)
![img](./Orion files/Images/8.png)
4. Close Launcher and reopen with Admin rights
5. Apply Updates 
![img](./Orion files/Images/4.png)
6. Configure Client version (From original client.exe)
7. Rename server (double click)
![img](./Orion files/Images/5.png)
8. Edit default profile
![img](./Orion files/Images/6.png)
9. Launch client
10. Create new Profile in Paperdol options
![img](./Orion files/Images/9.png)
11. In Erebor/OrionGlobalProfiles open created profile folder and paste options.cfg and gumps.xml with spell icons [files](./Orion files/Profiles/)
12. Paste vegetation.txt and stumps.txt to Erebor/OrionData/ [files](./Orion files/Profiles/)
13. Paste OA/GlobalConfig/FindList.xml to Erebor/OA/GlobalConfig
[files](./Orion files/Profiles/)
14. Download UOAM patch and rewrite files in Erebor/UOAM and run uoam_Orion with orion :-)
![img](./Orion files/Images/7.png)
15. Set up Assistant Main/Main tab
![img](./Orion files/Images/0.png)
16. Set up Assistant Main/Options tab
![img](./Orion files/Images/1.png)
17. Set Up Display tab (click button Set tittle from goup -> default) for show info in titlebar
![img](./Orion files/Images/2.png)
18. Hotkeys -> load examples and modified in "Open hotkeys dialog" after changes safe
![img](./Orion files/Images/11.png)
19. Scripts tab yellow icon for open autoload.oajs
![img](./Orion files/Images/3.png)


Script objects:
1. Lists/Objects tab click on green +
2. Name "carv" after click on magnitifier and target item for carving dead bodies
3. Add another name "lotBag" and target lot backpack
![img](./Orion files/Images/12.png)
4. Lists/Friends add characters for autohealing and skip them while targeting
![img](./Orion files/Images/13.png)
5. Set autoload.oajs parameters copy CharacterSettings['TEMPLATE'] and edit for character name 
    -> CharacterSettings['CharName']  { }
![img](./Orion files/Images/14.png)

In Game tips:
1. Press Shift unlock stacked spell icons
2. Press ctrl+alt lock/unlock gumps
![img](./Orion files/Images/10.png)
3. Press ctrl+shift show all objects in world


Scripting:
[Wiki documentation](https://github.com/Hotride/OrionUO/wiki/OrionUO-wiki-in-English)
In assistant edit sc
![img](./Orion files/Images/15.png)